
package vista;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.logging.Level;
import java.util.logging.Logger;
import modelo.Alumno;

/**
 * Fichero: Vista.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 21-oct-2014
 */
public class Vista {

  public Alumno getAlumno() {

    boolean error;
      
    InputStreamReader isr = new InputStreamReader(System.in);
    BufferedReader br = new BufferedReader(isr);

    Alumno a;
    String nombre = "";
    String linea = "";
    int edad = 0;

    System.out.println("ENTRADA DE DATOS.");
    System.out.print("Nombre: ");
    try {
      nombre = br.readLine();
    } catch (IOException ex) {
      Logger.getLogger(Vista.class.getName()).log(Level.SEVERE, null, ex);
    }

    
    do  {           
    System.out.print("Edad: ");
    try {
      linea = br.readLine();

    } catch (IOException ex) {
      Logger.getLogger(Vista.class.getName()).log(Level.SEVERE, null, ex);
    } 
    
    error=false;
    try {
       edad = Integer.parseInt(linea);
    }
    catch ( NumberFormatException nfe ) {
        error=true;
        System.out.println("Error: Edad es numerico");
    }
    
    } while (error);// while
  
    a = new Alumno(nombre, edad);

    return a;
  }

  public void showAlumno(Alumno a) {
    System.out.println("DATOS ALUMNO.");
    System.out.println("Nombre: " + a.getNombre());
    System.out.println("Edad: " + a.getEdad());
  }

    public void mostrarMediaEdad(String texto) {
        System.out.println(texto);
    }

}
