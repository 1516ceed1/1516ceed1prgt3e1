
package controlador;

import modelo.Alumno;
import vista.Vista;

/**
 * Fichero: Main.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 21-oct-2014
 */
public class Main {

  /**
   * @param args the command line arguments
   */
  public static void main(String[] args) {

    Alumno a = new Alumno();
    Vista v = new Vista();
    int suma = 0;
    int num = 3;
    double media = 0.0;
    
    for (int i=1; i<=num; i++){
       a = v.getAlumno();
       suma = suma + a.getEdad();
       //v.showAlumno(a);
    }
    media = suma/num;
     v.mostrarMediaEdad("La media de edad es: "+media);
 
  }

}
